

const iGoogle = 'assets/icons/icon_google.svg';
const iFacebook = 'assets/icons/icon_facebook.svg';
const iSearch = 'assets/icons/icon_search_normal.svg';
const iCamera = 'assets/icons/icon_camera.svg';
const iAdidasBlack = 'assets/icons/icon_adidas_black.svg';
const iJordanBlack = 'assets/icons/icon_jordan_black.svg';
const iNikeBlack = 'assets/icons/icon_nike_black.svg';
const iPumaBlack = 'assets/icons/icon_puma_black.svg';
const iReebokBlack = 'assets/icons/icon_reebok_black.svg';
const iVansBlack = 'assets/icons/icon_vans_black.svg';
const iAdidasGrey = 'assets/icons/icon_adidas_grey.svg';
const iJordanGrey = 'assets/icons/icon_jordan_grey.svg';
const iNikeGrey = 'assets/icons/icon_nike_grey.svg';
const iPumaGrey = 'assets/icons/icon_puma_grey.svg';
const iReebokGrey = 'assets/icons/icon_reebok_grey.svg';
const iVansGrey = 'assets/icons/icon_vans_grey.svg';
const iHeartOn = 'assets/icons/icon_heart_on.svg';
const iHeartOff = 'assets/icons/icon_heart_off.svg';
const iStarsSmallOn = 'assets/icons/icon_stars_small_on.svg';
const iHomeOn = 'assets/icons/icon_home_on.svg';
const iHomeOff = 'assets/icons/icon_home_off.svg';
const iDiscoverOn = 'assets/icons/icon_discover_on.svg';
const iDiscoverOff = 'assets/icons/icon_discover_off.svg';
const iOrderOn = 'assets/icons/icon_order_on.svg';
const iOrderOff = 'assets/icons/icon_order_off.svg';
const iProfileOn = 'assets/icons/icon_profile_on.svg';
const iProfileOff = 'assets/icons/icon_profile_off.svg';
const iBag = 'assets/icons/icon_bag.svg';
const iArrowLeft = 'assets/icons/icon_arrow_left.svg';

const apiEndpoint = 'https://randomuser.me/api/';


const imgOnboarding1 = 'assets/images/onboarding1.png';
const imgOnboarding2 = 'assets/images/onboarding2.png';
const imgOnboarding3 = 'assets/images/onboarding3.png';

const createaccountsuccess = 'assets/images/createaccountsuccess.png';
const homepageBanner = 'assets/images/homepage_banner.png';

const special1 = 'assets/images/special1.png';
const special2 = 'assets/images/special2.png';
const special3 = 'assets/images/special3.png';
const special4 = 'assets/images/special4.png';

const popular1 = 'assets/images/popular1.png';
const popular2 = 'assets/images/popular2.png';
const popular3 = 'assets/images/popular3.png';

class TypeShoesImage{ 
  static const casual = 'assets/images/training_shoe.png';
  static const basketball = 'assets/images/basketball_shoe.png';
  static const football = 'assets/images/football_shoe.png';
  static const running = 'assets/images/running_shoe.png';
  static const training = 'assets/images/training_shoe.png';
}

class Product{
  static const productAdidas1 = 'assets/images/product_adidas1.png';
  static const productAdidas2 = 'assets/images/product_adidas2.png';
  static const productAdidas3 = 'assets/images/product_adidas3.png';

  static const productJordan1 = 'assets/images/product_jordan1.png';
  static const productJordan2 = 'assets/images/product_jordan2.png';

  static const productNike1 = 'assets/images/product_nike1.png';
  static const productNike2 = 'assets/images/product_nike2.png';
  static const productNike3 = 'assets/images/product_nike3.png';
  static const productNike4 = 'assets/images/product_nike4.png';
  static const productNike5 = 'assets/images/product_nike5.png';

}