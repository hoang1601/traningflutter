
class User {
  final String name;
  final String address;
  final String imagePath;
  final int age;

  User(this.name, this.address, this.imagePath, this.age);

  User.fromJson(Map<String, dynamic> json)
      : name = json['name']['first'],
        address = json['location']['state'] + ", " + json['location']['country'],
        imagePath = json['picture']['large'],
        age = json['dob']['age'] ;


}