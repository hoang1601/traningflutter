import 'package:flutter/material.dart';
import 'package:hoang_dinh/src/features/profile/profile_screen.dart';
import 'package:hoang_dinh/src/features/profile/profile_viewmodel.dart';
import 'package:hoang_dinh/src/service/profile_service.dart';
import 'package:provider/provider.dart';

class FeatureFactory {
  static Widget create<T>() {
    switch (T) {
      case Profile:
        return ChangeNotifierProvider(
          create: (_) => ProfileViewModel(ProfileService()),
          builder: (_, __) => const Profile(),
        );
    }

    return const Scaffold();
  }


  
}
