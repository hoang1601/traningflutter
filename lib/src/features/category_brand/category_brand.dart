import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hoang_dinh/src/constants/constans.dart';
import 'package:hoang_dinh/src/features/custom_widget/item_special.dart';

class CategoryBrand extends StatefulWidget {
  const CategoryBrand({Key? key, required this.index}) : super(key: key);
  final int index;

  @override
  State<CategoryBrand> createState() => _CategoryBrandState();
}

class _CategoryBrandState extends State<CategoryBrand> {
  late int index;
  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    index = widget.index;
    _scrollController = ScrollController(initialScrollOffset: (120*index).toDouble());
  }
  void selectCategory(int i){
    setState(() {
      index = i;
    });
  }

  void onTapBack(){
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
             //Title Icons
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
              padding: const EdgeInsets.symmetric(vertical: 17),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[                
                  InkWell(
                    onTap: onTapBack,
                    child: SvgPicture.asset(
                      iArrowLeft,
                      width: 24,
                      height: 24,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SvgPicture.asset(
                        iSearch,
                        width: 24,
                        height: 24,
                      ),
                      const SizedBox(width: 20,),
                      SvgPicture.asset(
                        iBag,
                        width: 24,
                        height: 24,
                      ),
                    ],
                  ),

                  
                ],
              ),
            ),
            //Title Category
            Container(
              margin: const EdgeInsets.only(left: 30,bottom: 30),
              child: SingleChildScrollView(
                controller: _scrollController,
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () => selectCategory(0),
                      child: TextCategory(brandName: "Casual Shoes", isSelect: index == 0)
                    ),
                    InkWell(
                      onTap: () => selectCategory(1),
                      child: TextCategory(brandName: "Running Shoes", isSelect: index == 1)
                    ),
                    InkWell(
                      onTap: () => selectCategory(2),
                      child: TextCategory(brandName: "Basketball Shoes", isSelect: index == 2)
                    ),
                    InkWell(
                      onTap: () => selectCategory(3),
                      child: TextCategory(brandName: "Training Shoes", isSelect: index == 3)
                    ),
                    InkWell(
                      onTap: () => selectCategory(4),
                      child: TextCategory(brandName: "Football Shoes", isSelect: index == 4)
                    ),

                  ],
                ),
              ),
            ),
            //Product
            Expanded(
              child: GridView.count(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                crossAxisCount: MediaQuery.of(context).size.width > 555 ? 3 : 2,
                childAspectRatio: 150/224,
                children: const <Widget>[
                  SpecialItem(iBrand: iNikeGrey, imagePath: Product.productNike1, itemName: "Jordan 1 Retro High Tie Dye", stars: "4.8", review: "58", money: "259,00",),
                  SpecialItem(iBrand: iNikeGrey, imagePath: Product.productNike2, itemName: "Jordan 1 Retro High Tie Dye", stars: "3.5", review: "563", money: "256,00",),
                  SpecialItem(iBrand: iNikeGrey, imagePath: Product.productNike3, itemName: "Jordan 1 Retro High Tie Dye", stars: "2.6", review: "2156", money: "166,00",),
                  SpecialItem(iBrand: iNikeGrey, imagePath: Product.productNike4, itemName: "Jordan 1 Retro High Tie Dye", stars: "4.1", review: "698", money: "275,00",),
                  SpecialItem(iBrand: iNikeGrey, imagePath: Product.productNike5, itemName: "Jordan 1 Retro High Tie Dye", stars: "4.5", review: "735", money: "169,00",),
                  SpecialItem(iBrand: iNikeGrey, imagePath: Product.productNike1, itemName: "Jordan 1 Retro High Tie Dye", stars: "4.9", review: "658", money: "328,00",),
                ],
              )
            ),
            
          ],
        ),
      ),
    );
  }
}

class TextCategory extends StatelessWidget {
  const TextCategory({
    Key? key, required this.brandName, required this.isSelect,
  }) : super(key: key);
  final String brandName;
  final bool isSelect;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 20),
      child: Text(
        brandName,
        style: TextStyle(
          fontWeight:  FontWeight.w700,
          fontSize: 20,
          letterSpacing: 1,
          color: isSelect? Colors.black : const Color(0xFFB7B7B7),
        ),
      ),
    );
  }
}