import 'package:flutter/material.dart';
import 'package:hoang_dinh/src/constants/constans.dart';
import 'package:hoang_dinh/src/features/create_account/create_account_screen.dart';
import 'package:hoang_dinh/src/features/custom_widget/mybutton_social.dart';
import 'package:hoang_dinh/src/features/custom_widget/textbutton_custom.dart';


class SignUpScreen extends StatefulWidget{
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SignUpScreen();
}

class _SignUpScreen extends State<SignUpScreen>{
  
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _showpassword = false;

  void _onTapGoogle(){
    print("Tap Google");
  }

  void _onTapFacebook(){
    print("Tap Facebook");
  }
  void _onTapSignUp()
  {
    print("SignUp");
  }

  void _onTapCreateAccount()
  {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const CreateAccountScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      backgroundColor: const Color(0xFFFFFFFF),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          //Sign Up Title
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 30).copyWith(top: 50),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Sign In",
                  style: Theme.of(context).textTheme.headline4,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  padding: const EdgeInsets.only(top: 10),
                  child:  Text(
                    "Please sign in to your Shoesly Account",
                    style: Theme.of(context).textTheme.bodyText1?.copyWith(color: const Color(0xFF6F6F6F),)
                    ),
                ),
              ],
            ),
          ),
          
          //TextForm
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 30),
            height: 186,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                TextFormField(
                  style: const TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    letterSpacing: 1,
                  ),
                  controller: _emailController,
                  decoration: InputDecoration(                     
                    labelText: "Email",
                    labelStyle: const TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                      letterSpacing: 1,
                      color: Colors.black
                    ),
                    floatingLabelStyle: const TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                      letterSpacing: 1,
                      color: Colors.black
                    ),
                    border: OutlineInputBorder(
                      borderSide: const BorderSide(width: 1, color: Colors.black),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    contentPadding: const EdgeInsets.only(top:10, left: 10),
                    hintText: "pristia@gmail.com",
                  ),
                ),
                TextFormField(
                  style: const TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    letterSpacing: 1,
                  ),
                  controller: _passwordController,
                  obscureText: _showpassword,
                  decoration: InputDecoration(
                    labelText: "Password",
                    labelStyle: const TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                      letterSpacing: 1,
                      color: Colors.black
                    ),
                    floatingLabelStyle: const TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                      letterSpacing: 1,
                      color: Colors.black
                    ),
                    border: OutlineInputBorder(
                      borderSide: const BorderSide(width: 1, color: Colors.black),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    contentPadding: const EdgeInsets.only(top:10, left: 10),
                    hintText: "secret1234567",
                    suffixIcon: IconButton(
                      icon: Icon(_showpassword ? Icons.visibility :Icons.visibility_off), 
                      onPressed: () {  
                        setState(() {
                          _showpassword = !_showpassword;
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
          
          //Sign In Button           
          
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 30,),
            child: GestureDetector(
                onTap: _onTapSignUp,
                child: TextButtonCustom(text: 'Sign In'.toUpperCase(),),
            ),
          ),
          // Social Button
          Container(
            margin: const EdgeInsets.symmetric(vertical : 20),
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              children: <Widget> [
                Expanded(
                  child: GestureDetector(
                    onTap: () => _onTapGoogle(),
                    child: const MyButtonSocial(
                      icon: iGoogle,
                      text: 'Google',
                    ),
                  )
                ),
                const SizedBox(width: 15),
                Expanded(
                  child: GestureDetector(
                    onTap: () => _onTapFacebook(),
                    child: const MyButtonSocial(
                      icon: iFacebook,
                      text: 'Facebook',
                    ),
                  )
                ),
              ],
            ),
          ),
          //Join with us Title
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 30),
            padding: const EdgeInsets.symmetric(vertical: 10),
            child:  Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Join with us,",
                  style: Theme.of(context).textTheme.bodyText1?.copyWith(color: const Color(0xFF6F6F6F)),
                ),
                GestureDetector(
                  onTap: _onTapCreateAccount,
                  child: Text(
                    " Create Account",
                    style: Theme.of(context).textTheme.bodyText1?.copyWith(fontWeight: FontWeight.w600),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );

  }
}


