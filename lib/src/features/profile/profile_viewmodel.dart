
import 'package:flutter/cupertino.dart';
import 'package:hoang_dinh/src/model/User.dart';
import 'package:hoang_dinh/src/service/profile_service.dart';

class ProfileViewModel extends ChangeNotifier{
  final ProfileService _profileService;

  ProfileViewModel(this._profileService);

  User? user;

  Future<void> loadDataUser(String url) async{
    user = await _profileService.getDataUser(url);
    notifyListeners();
  }
}