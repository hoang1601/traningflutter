

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hoang_dinh/src/constants/constans.dart';
import 'package:hoang_dinh/src/features/discover/discover.dart';
import 'package:hoang_dinh/src/features/homepage/homepage_screen.dart';
import 'package:hoang_dinh/src/features/order/order.dart';
import 'package:hoang_dinh/src/features/wishlist/wisthlist.dart';
import 'package:provider/provider.dart';

import 'profile_viewmodel.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {


  @override
  void initState() {
    context.read<ProfileViewModel>().loadDataUser(apiEndpoint);
    super.initState();
  }

  void _onTapNavigator(int i){
    switch (i) {
      case 1:
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const Discover()),
        );
        break;
      case 2:
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const Order()),
        );
        break;
      case 3:
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const WishList()),
        );
      break;
      case 4:
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const Profile()),
        );
      break;
      default:
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => const HomePage()),
        );
        break;
    }
  }


 @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SafeArea(
        child: Consumer<ProfileViewModel>(
          builder: (context, value, child){
            if(value.user == null)
            {
              return const CircularProgressIndicator();
            }
            else{
              return Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [               
                      Padding(
                        padding: const EdgeInsets.only(top:20),
                        child: CircleAvatar(
                          radius: 50,
                          backgroundImage: NetworkImage(value.user!.imagePath),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:20),
                        child: Text(
                          "Hello, ${value.user!.name}",
                          style: Theme.of(context).textTheme.headline4,
                        ),
                      ),
                      Text(
                          value.user!.address,
                          style: Theme.of(context).textTheme.bodyText1?.copyWith(color: const Color(0xFF727A83)),
                      ),

                      Container(
                        margin: const EdgeInsets.all(30),
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: const Color(0xFF101010),
                          borderRadius: BorderRadius.circular(100),
                        ),
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Text(
                                "ON GOING ORDER",
                                style: Theme.of(context).textTheme.headline6!.copyWith(color: Colors.white),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.red,
                                  borderRadius: BorderRadius.circular(100),
                                ),
                                padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 2),
                                child: Center(
                                  child: Text(
                                    value.user!.age.toString(),
                                    style: Theme.of(context).textTheme.headline6!.copyWith(color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Expanded(child: SizedBox()),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Detail Profile",
                              style: Theme.of(context).textTheme.headline5,
                            ),
                            const Icon(Icons.keyboard_arrow_right_outlined)
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Setting",
                              style: Theme.of(context).textTheme.headline5,
                            ),
                            const Icon(Icons.keyboard_arrow_right_outlined)
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Language",
                              style: Theme.of(context).textTheme.headline5,
                            ),
                            const Icon(Icons.keyboard_arrow_right_outlined)
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Currency",
                              style: Theme.of(context).textTheme.headline5,
                            ),
                            const Icon(Icons.keyboard_arrow_right_outlined)
                          ],
                        ),
                      ),
                      const Expanded(flex: 2,child: SizedBox()),
                  ],
                ),
              );
            }
          },
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          padding: const EdgeInsets.all(30),
          height: 80,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GestureDetector(
                onTap: () => _onTapNavigator(0),
                child: SvgPicture.asset(
                  iHomeOff,
                  width: 20,
                  height: 20,
                ),
              ),
              GestureDetector(
                onTap: () => _onTapNavigator(1),
                child: SvgPicture.asset(
                  iDiscoverOff,
                  width: 20,
                  height: 20,
                ),
              ),
              GestureDetector(
                onTap: () => _onTapNavigator(2),
                child: SvgPicture.asset(
                  iOrderOff,
                  width: 20,
                  height: 20,
                ),
              ),
              GestureDetector(
                onTap: () => _onTapNavigator(3),
                child: SvgPicture.asset(
                  iHeartOff,
                  width: 20,
                  height: 20,
                ),
              ),
              SvgPicture.asset(
                iProfileOn,
                width: 20,
                height: 20,
              ),
              
            ],
          ),
        ),
      ),
    
    );
  }
}