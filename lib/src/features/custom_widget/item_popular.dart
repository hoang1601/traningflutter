import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hoang_dinh/src/constants/constans.dart';

class PopularItem extends StatefulWidget {
  const PopularItem({
    Key? key, required this.imagePath, required this.iBrand, required this.itemName, required this.stars, required this.review, required this.money,
  }) : super(key: key);

  final String imagePath;
  final String iBrand;
  final String itemName;
  final String stars;
  final String review;
  final String money;

  @override
  State<PopularItem> createState() => _PopularItemState();
}

class _PopularItemState extends State<PopularItem> {

  bool isSelect = false;

  void onTapGestureDetector()
  {
    setState(() {
      isSelect = !isSelect;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: const Color(0xFFF3F3F3),
            ),
            padding: const EdgeInsets.symmetric(vertical: 19,horizontal: 10),
            child: Image.asset(
              widget.imagePath,
              width: 70,
              height: 50,
            ),
          ),

          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SvgPicture.asset(
                        widget.iBrand,
                        height: 22,
                        width: 24,
                      ),
          
                      GestureDetector(
                        onTap: onTapGestureDetector,
                        child: SvgPicture.asset(
                          isSelect ? iHeartOn : iHeartOff,
                          height: 22,
                          width: 24,
                        ),
                      )
                    ],
                  ),
                  
                  Container(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: Text(
                      widget.itemName,
                      style: const TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                        letterSpacing: 1,
                      ),
                    ),
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SvgPicture.asset(
                        iStarsSmallOn,
                        width: 10,
                        height: 10,
                      ),
                      Text(
                        "${widget.stars}  ",
                        style: const TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 11,
                          letterSpacing: 1,
                        ),
                      ),
                      Text(
                        "(${widget.review} Reviews)",
                        style: const TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 11,
                          letterSpacing: 1,
                          color: Color(0xFFB7B7B7),
                        ),
                      ),
                    ],
                  ),

                  Container(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: Text(
                      "\$ ${widget.money}",
                      style: const TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 14,
                        letterSpacing: 1,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

