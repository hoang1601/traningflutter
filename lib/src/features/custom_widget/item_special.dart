import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hoang_dinh/src/constants/constans.dart';

class SpecialItem extends StatefulWidget {
  const SpecialItem({
    Key? key, required this.iBrand, required this.imagePath, required this.itemName, required this.stars, required this.review, required this.money,
  }) : super(key: key);
  final String iBrand;
  final String imagePath;
  final String itemName;
  final String stars;
  final String review;
  final String money;

  @override
  State<SpecialItem> createState() => _SpecialItemState();
}

class _SpecialItemState extends State<SpecialItem> {

  bool isSelect = false;
  void onTapGestureDetector()
  {
    setState(() {
      isSelect = !isSelect;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 20),
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(25),
                  color: const Color(0xFFF3F3F3),
                ),
                padding: const EdgeInsets.all(15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: 120.0,
                      padding:const EdgeInsets.only(bottom: 4),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SvgPicture.asset(
                            widget.iBrand,
                            width: 10,
                            height: 10,
                          ),
                          GestureDetector(
                            onTap: onTapGestureDetector,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(25),
                                color: Colors.white,
                              ),
                              padding: const EdgeInsets.all(6),
                              child: SvgPicture.asset(
                                isSelect ? iHeartOn : iHeartOff,
                                width: 12,
                                height: 11,
                              ),
                            ),
                          ),
                        ],                              
                      ),
                    ),
                    Image.asset(
                      widget.imagePath,
                      width: 120.0,
                      height: 85.0,
                    ),
                  ],
                ),
              ),

              Container(
                width: 150,
                padding: const EdgeInsets.only(top: 10),
                margin: const EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  widget.itemName,
                  style: const TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                    letterSpacing: 1,
                  ),
                ),
              ),
              
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SvgPicture.asset(
                    iStarsSmallOn,
                    width: 10,
                    height: 10,
                  ),
                  Text(
                    "${widget.stars}  ",
                    style: const TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 11,
                      letterSpacing: 1,
                    ),
                  ),
                  Text(
                    "(${widget.review} Reviews)",
                    style: const TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 11,
                      letterSpacing: 1,
                      color: Color(0xFFB7B7B7),
                    ),
                  ),
                ],
              ),
              
              Container(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Text(
                  "\$ ${widget.money}",
                  style: const TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 14,
                    letterSpacing: 1,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}


