import 'package:flutter/material.dart';

class HomePageCategory extends StatelessWidget {
  const HomePageCategory({
    Key? key, required this.title, this.callback,
  }) : super(key: key);
  final String title;
  final VoidCallback? callback;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 30, right: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            title,
            style: Theme.of(context).textTheme.headline5,
          ),

          GestureDetector(
            onTap: () => callback,
            child: const Text(
              "SEE ALL",
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 12,
                letterSpacing: 1,
                color: Color(0xFFB7B7B7),                        
              ),
            ),
          )
        
        ],
      ),
    );
  }
}
