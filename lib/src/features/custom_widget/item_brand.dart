import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ItemBrands extends StatelessWidget {
  const ItemBrands({
    Key? key, required this.icon, required this.name, required this.quantity,
  }) : super(key: key);
  final String icon;
  final String name;
  final String quantity;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 32),
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              color: const Color(0xFFF3F3F3),
            ),
            padding: const EdgeInsets.all(10),
            child: SvgPicture.asset(
              icon,
              width: 30.0,
              height: 30.0,
            ),
          ),

          Container(
            padding: const EdgeInsets.only(top: 10),
            child: Text(
              name,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyText1?.copyWith(fontWeight: FontWeight.w700),
            ),
          ),
          
          Text(
            "$quantity Items",
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyText1?.copyWith(fontSize: 11, color: const Color(0xFFB7B7B7)),
            
           ),
        
        ],
      ),
    );
  }
}