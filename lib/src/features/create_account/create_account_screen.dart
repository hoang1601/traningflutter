import 'package:flutter/material.dart';
import 'package:hoang_dinh/src/constants/constans.dart';
import 'package:hoang_dinh/src/features/create_account/success_create_account_screen.dart';
import 'package:hoang_dinh/src/features/custom_widget/mybutton_social.dart';
import 'package:hoang_dinh/src/features/custom_widget/textbutton_custom.dart';
import 'package:hoang_dinh/src/features/sign_up/sign_up_screen.dart';


class CreateAccountScreen extends StatefulWidget{
  const CreateAccountScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CreateAccountScreen();
}

class _CreateAccountScreen extends State<CreateAccountScreen>{
  
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordConfirmController = TextEditingController();
  bool _showPassword = false;
  bool _showConfirmPassword = false;
  final double _spaceBetweenField = 30;
  bool _checkBox = false;

  void _onTapCreateAccount()
  {
        Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => const SuccessCreateAccountScreen()),
    );
  }
  
  void _onTapGoogle(){
    print("Tap Google");
  }

  void _onTapFacebook(){
    print("Tap Facebook");
  }

  void _onTapSignUp(){
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => const SignUpScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      backgroundColor: const Color(0xFFFFFFFF),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //Create Account Title
            Container(
              height: 79,
              margin: const EdgeInsets.only(left: 30,top: 20, right: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 315,
                    height: 45,
                    child: Text(
                      "Create Account",
                      style: Theme.of(context).textTheme.headline4
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(top: 10),
                    child:  Text(
                      "Please sign up to your Shoesly Account",
                      style: Theme.of(context).textTheme.bodyText1?.copyWith(color: const Color(0xFF6F6F6F)),
                      ),
                  ),
                ],
              ),
            ),
            
            // TextForm
            Container(
              margin: const EdgeInsets.only(left: 30,top: 38, right: 30),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  //Name TextField
                  TextFieldNoObscure(label: "Name",hint: "Pristia Candra", controller: _nameController),
                  SizedBox(
                    height: _spaceBetweenField,
                  ),
      
                  //Email TextField
                  TextFieldNoObscure(label: "Email",hint: "pristia@gmail.com", controller: _emailController),
                  SizedBox(
                    height: _spaceBetweenField,
                  ),
      
                  //Password TextField
                  TextFormField(
                    style: const TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      letterSpacing: 1,
                    ),
                    controller: _passwordController,
                    obscureText: _showPassword,
                    decoration: InputDecoration(
                      labelText: "Password",
                      labelStyle: const TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 14,
                        letterSpacing: 1,
                        color: Colors.black
                      ),
                      floatingLabelStyle: const TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 14,
                        letterSpacing: 1,
                        color: Colors.black
                      ),
                      border: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.black),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      contentPadding: const EdgeInsets.only(top:10, left: 10),
                      hintText: "secret1234567",
                      suffixIcon: IconButton(
                        icon: Icon(_showPassword ? Icons.visibility :Icons.visibility_off), 
                        onPressed: () {  
                          setState(() {
                            _showPassword = !_showPassword;
                          });
                        },
                      ),
                    ),
                  ),
      
                  //Text below password
                  Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(top: 10),
                    child: Text(
                      "Your password must 8 or more characters ",
                      textAlign: TextAlign.start,
                      style: Theme.of(context).textTheme.bodyText2?.copyWith(color: const Color(0xFF6F6F6F)),
                    ),
                  ),
                  SizedBox(height: _spaceBetweenField,),
                  
                  //Confirm Password TextField
                  TextFormField(
                    style: const TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      letterSpacing: 1,
                    ),
                    controller: _passwordConfirmController,
                    obscureText: _showConfirmPassword,
                    decoration: InputDecoration(
                      labelText: "Confirm Password",
                      labelStyle: const TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 14,
                        letterSpacing: 1,
                        color: Colors.black
                      ),
                      floatingLabelStyle: const TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 14,
                        letterSpacing: 1,
                        color: Colors.black
                      ),
                      border: OutlineInputBorder(
                        borderSide: const BorderSide(width: 1, color: Colors.black),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      contentPadding: const EdgeInsets.only(top:10, left: 10),
                      hintText: "secret1234567",
                      suffixIcon: IconButton(
                        icon: Icon(_showConfirmPassword ? Icons.visibility :Icons.visibility_off), 
                        onPressed: () {  
                          setState(() {
                            _showConfirmPassword = !_showConfirmPassword;
                          });
                        },
                      ),
                    ),
                  ),                 
                ],
              ),
            ),
            
            // Checkbox
            Container(
              margin: const EdgeInsets.only(top: 30, bottom: 50),
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Row(
                children: <Widget>[
                    SizedBox(
                      width: 20,
                      height: 20,
                      child: Checkbox(
                        activeColor: Colors.black,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                        ), 
                        onChanged: (value) { 
                          setState(() {
                            _checkBox = value!;    
                          });
                        }, 
                        value: _checkBox,
                      ),
                    ),
                    Flexible(
                      child: Container(
                        padding: const EdgeInsets.only(left: 12),
                        child: RichText(
                          text: const TextSpan(
                            text: "By Signing up, you agree to the ",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 12,
                              letterSpacing: 1,
                              color: Colors.black,                              
                            ),
                            children: [
                              TextSpan(
                                text: "Terms of Service ",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                  letterSpacing: 1,
                                  color: Colors.black,
                                ),
                              ),
                              TextSpan(
                                text: "and ",
                                style: TextStyle(
                                  fontSize: 12,
                                  letterSpacing: 1,
                                  color: Colors.black,
                                ),
                              ),
                              TextSpan(
                                text: "Privacy Policy",
                                  style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                  letterSpacing: 1,
                                  color: Colors.black,
                                ),
                              ),
                            ]
      
                          ),
                          
                        ),
                      ),
                    ),
                ]
              )
            ),
            
            //Create Account Button           
            Container(
              padding: const EdgeInsets.only(bottom: 20),
              margin: const EdgeInsets.symmetric(horizontal: 30,),
              child: GestureDetector(
                  onTap: _onTapCreateAccount,
                  child: TextButtonCustom(text: 'Create Account'.toUpperCase(),),
              ),
            ),
      
            // Social Button
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () => _onTapGoogle(),
                        child: const MyButtonSocial(
                          icon: iGoogle,
                          text: 'Google',
                        ),
                      )
                    ),
                    const SizedBox(width: 15),
                    Expanded(
                      child: GestureDetector(
                        onTap: () => _onTapFacebook(),
                        child: const MyButtonSocial(
                          icon: iFacebook,
                          text: 'Facebook',
                        ),
                      )
                    ),
                  ],
                ),
              ),
            
            //Join with us Title
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 30),
              padding: const EdgeInsets.symmetric(vertical: 10),
              child:  Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text(
                    "Have Account? ",
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      letterSpacing: 1
                    ),
                  ),
                  GestureDetector(
                    onTap: _onTapSignUp,
                    child: const Text(
                      "Sign Up Now",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 16.8,
                        letterSpacing: 1
                      ),
                    ),
                  ),
                ],
              ),
            ),
          
          ],
        ),
      ),
    );

  }
}

class TextFieldNoObscure extends StatelessWidget {
  const TextFieldNoObscure({
    Key? key,
    required TextEditingController controller, required this.label, required this.hint,
  }) : _controller = controller, super(key: key);

  final String label;
  final String hint;
  final TextEditingController _controller;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      style: const TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: 14,
        letterSpacing: 1,
      ),
      controller: _controller,
      decoration: InputDecoration(                     
        labelText: label,
        labelStyle: const TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: 14,
          letterSpacing: 1,
          color: Colors.black
        ),
        floatingLabelStyle: const TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: 14,
          letterSpacing: 1,
          color: Colors.black
        ),
        border: OutlineInputBorder(
          borderSide: const BorderSide(width: 1, color: Colors.black),
          borderRadius: BorderRadius.circular(20),
        ),
        contentPadding: const EdgeInsets.only(top:10, left: 10),
        hintText: hint,
      ),
    );
  }
}


