import 'package:flutter/material.dart';
import 'package:hoang_dinh/src/constants/constans.dart';
import 'package:hoang_dinh/src/features/custom_widget/textbutton_custom.dart';
import 'package:hoang_dinh/src/features/onboarding/onboarding_description.dart';
import 'package:hoang_dinh/src/features/tabbar/tabbar.dart';

class SuccessCreateAccountScreen extends StatefulWidget {
  const SuccessCreateAccountScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<SuccessCreateAccountScreen> createState() => _SuccessCreateAccountScreenState();
}

class _SuccessCreateAccountScreenState extends State<SuccessCreateAccountScreen> {

  void _onTapSignUp(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const TabBarScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFFFFFF),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              // Description
              const SizedBox(
                height: 540,
                child: OnBoardingDescription(imagepath: createaccountsuccess, content: 'Welcome To The Club Of Shoesly!', subcontent: 'Your account has been created success',),                          
              ),
              // Get Started Button
              Container(
                margin: const EdgeInsets.only(top: 30),
                padding: const EdgeInsets.symmetric(horizontal: 30)
                    .copyWith(bottom: 20),
                child: GestureDetector(
                  onTap: _onTapSignUp,
                  child: TextButtonCustom(text: 'Get Started'.toUpperCase(),),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


