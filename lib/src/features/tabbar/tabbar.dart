
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hoang_dinh/src/constants/constans.dart';
import 'package:hoang_dinh/src/features/discover/discover.dart';
import 'package:hoang_dinh/src/features/homepage/homepage_screen.dart';
import 'package:hoang_dinh/src/features/order/order.dart';
import 'package:hoang_dinh/src/features/profile/profile_screen.dart';
import 'package:hoang_dinh/src/features/wishlist/wisthlist.dart';

class TabBarScreen extends StatefulWidget {
  const TabBarScreen({Key? key}) : super(key: key);

  @override
  State<TabBarScreen> createState() => _TabBarScreen();
}

class _TabBarScreen extends State<TabBarScreen> with SingleTickerProviderStateMixin {
  late final TabController _tabController = TabController(vsync: this, length: 5);

  @override
  void initState() {
    super.initState();
    _tabController.addListener(_handleTabSelection);
  }
  
  void _handleTabSelection() {
    setState(() {
      });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        body: TabBarView(
          controller: _tabController,
          children: const [
            HomePage(),
            Discover(),
            Order(),
            WishList(),
            Profile()
          ],
        ),
        bottomNavigationBar: TabBar(
        controller: _tabController,
        tabs: [
          Tab(icon: SvgPicture.asset(_tabController.index == 0 ? iHomeOn : iHomeOff, width: 20,height: 20,),),
          Tab(icon: SvgPicture.asset(_tabController.index == 1 ? iDiscoverOn : iDiscoverOff, width: 20,height: 20,),),
          Tab(icon: SvgPicture.asset(_tabController.index == 2 ? iOrderOn : iOrderOff, width: 20,height: 20,),),
          Tab(icon: SvgPicture.asset(_tabController.index == 3 ? iHeartOn : iHeartOff, width: 20,height: 20,),),
          Tab(icon: SvgPicture.asset(_tabController.index == 4 ? iProfileOn : iProfileOff, width: 20,height: 20,),),
        ],
      ),
    ),
    );
  }
}

            