
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hoang_dinh/src/constants/constans.dart';
import 'package:hoang_dinh/src/features/brand_list/brand_list.dart';
import 'package:hoang_dinh/src/features/custom_widget/homepage_category.dart';
import 'package:hoang_dinh/src/features/custom_widget/item_brand.dart';
import 'package:hoang_dinh/src/features/custom_widget/item_popular.dart';
import 'package:hoang_dinh/src/features/custom_widget/item_special.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  double spaceBrandItem = 32;
  final TextEditingController _findcontroller = TextEditingController();

  void seeAllBrand()
  {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const BrandsList()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              //Search bar
              Container(
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xFFF3F3F3)),
                  borderRadius: BorderRadius.circular(100),
                ),
                margin: const EdgeInsets.symmetric(vertical: 30, horizontal: 30),
                padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 21),
                height: 54,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    SvgPicture.asset(
                      iSearch,
                      width: 16.0,
                      height: 16.0,
                    ),
                    Container (
                      padding: const EdgeInsets.only(top: 3),
                      width: 195,
                      height: 24,
                      child: TextField(
                        controller: _findcontroller,
                        decoration: const InputDecoration(
                          border: InputBorder.none,
                          hintText: "What are you looking for?",
                          hintStyle: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            letterSpacing: 1,
                            color: Color(0xFFB7B7B7),
                          ),
                        ),
                      ),
                    ),
                    SvgPicture.asset(
                      iCamera,
                      width: 16.0,
                      height: 16.0,
                    ),
                  ],
                ),
              ),
              
              //Banner Picture
              Container(
                margin: const EdgeInsets.all(30),
                height: 200,
                child: Stack(
                  children: <Widget>[
                    SizedBox(
                      width: 315,
                      height: 200,
                      child: Image.asset(
                        homepageBanner,
                        width: 315,
                        height: 200,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 20, bottom: 20),
                      height: 200,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          const Expanded(child: SizedBox()),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(6),
                              color: const Color(0xFF52BD94),
                            ),
                            padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 5),                
                            child: const Text(
                              "NEW", 
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 12,
                                letterSpacing: 1
                                ),
                              ),
                          ),
    
                          Container(
                            margin: const EdgeInsets.only(top:5),
                            child: const Text(
                              "Nike Air Max 98 Noir Black", 
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 20,
                                letterSpacing: 1
                              ),
                            ),
                          ),
    
                          Container(
                            margin: const EdgeInsets.only(top:5,bottom: 20),
                            child: const Text(
                              "\$ 89,99 USD", 
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 12,
                                letterSpacing: 1
                              ),
                            ),
                          ),
    
                          SizedBox(
                            width: 80,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  width: 40,
                                  height: 2,
                                  color: Colors.white,
                                ),
                                Container(
                                  width: 6,
                                  height: 2,
                                  color: const Color(0xFFFFFFFF).withOpacity(0.4),
                                ),
                                Container(
                                  width: 6,
                                  height: 2,
                                  color: const Color(0xFFFFFFFF).withOpacity(0.4),
                                ),
                                Container(
                                  width: 6,
                                  height: 2,
                                  color: const Color(0xFFFFFFFF).withOpacity(0.4),
                                ),
                                Container(
                                  width: 6,
                                  height: 2,
                                  color: const Color(0xFFFFFFFF).withOpacity(0.4),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
    
              //Brands Title
              HomePageCategory(title: "Brands", callback: seeAllBrand),
              
              //Brands Item
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Container(
                  height: 100,
                  margin: const EdgeInsets.only(top:20, bottom: 30, left: 30),
                  child: Row(
                    children: const <Widget> [
                      ItemBrands(icon: iNikeBlack, name:"NIKE", quantity: "1001",),
                      
                      ItemBrands(icon: iPumaBlack, name:"Puma", quantity: "601",),
                      
                      ItemBrands(icon: iAdidasBlack, name:"Adidas", quantity: "709",),
                      
                      ItemBrands(icon: iReebokBlack, name:"Reebok", quantity: "555",),
                      
                      ItemBrands(icon: iJordanBlack, name:"Jordan", quantity: "800",),
                      
                      ItemBrands(icon: iVansBlack, name:"Vans", quantity: "8456",),
                    ],
                  ),
                ),
              ),
            
              //Special Title
              const HomePageCategory(title: "Special For Your"),
              
              //Special Item
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Container(
                  height: 280,
                  margin: const EdgeInsets.only(top:20, bottom: 30, left: 30),
                  child: Row(
                    children: const <Widget> [
                      SpecialItem(iBrand: iNikeGrey, imagePath: special1, itemName: "Jordan 1 Retro High Tie Dye", stars: "4.5", review: "231", money: "235,00",),
                      SpecialItem(iBrand: iAdidasGrey, imagePath: special2, itemName: "Adidas Ozrah Shoes", stars: "4.2", review: "125", money: "260,00",),
                      SpecialItem(iBrand: iJordanGrey, imagePath: special3, itemName: "Air Jordan 1 Mid", stars: "4.8", review: "925", money: "319,00",),
                      SpecialItem(iBrand: iNikeGrey, imagePath: special4, itemName: "NikeCourt Zoom Vapor Cage 4 Rafa", stars: "4.4", review: "6295", money: "199,00",),
                    ],
                  ),
                ),
              ),
    
              //Most Popular Title
              const HomePageCategory(title: "Most Popular"),
              
              //Popular Item
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: const <Widget>[
                    PopularItem(imagePath: popular1, iBrand: iJordanGrey, itemName: "Jordan 1 Retro High Tie Dye", stars: "4.5", review: "526", money: "169.99",),
                    PopularItem(imagePath: popular2, iBrand: iAdidasGrey, itemName: "ULTRABOOST 5.0 DNA SHOES", stars: "4.1", review: "654", money: "152",),
                    PopularItem(imagePath: popular3, iBrand: iNikeGrey, itemName: "Nike Air Force 1 '07", stars: "43.2", review: "369", money: "256.99",),
                  ],
                ), 
              ),
    
            ],
          ),
        )   
       ),
    );
  }
}
