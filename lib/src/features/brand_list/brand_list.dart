
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hoang_dinh/src/constants/constans.dart';
import 'package:hoang_dinh/src/features/category_brand/category_brand.dart';

class BrandsList extends StatefulWidget {
  const BrandsList({Key? key}) : super(key: key);

  @override
  State<BrandsList> createState() => _BrandsListState();
}

class _BrandsListState extends State<BrandsList> {

  int index = 0 ;

  void selectBrand(int i){
    setState(() {
      index = i;
    });
  }

  void onTapBack(){
    Navigator.pop(context);
  }

  void onTapTypeShoe(int i){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CategoryBrand(index: i,)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            //Title Icons
            Container(
              margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 30),
              padding: const EdgeInsets.symmetric(vertical: 17),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  
                  InkWell(
                    onTap: onTapBack,
                    child: SvgPicture.asset(
                      iArrowLeft,
                      width: 24,
                      height: 24,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      SvgPicture.asset(
                        iSearch,
                        width: 24,
                        height: 24,
                      ),
                      const SizedBox(width: 20,),
                      SvgPicture.asset(
                        iBag,
                        width: 24,
                        height: 24,
                      ),
                    ],
                  ),

                  
                ],
              ),
            ),
            //Title Category Brands
            Container(
              margin: const EdgeInsets.only(left: 30,bottom: 30),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => selectBrand(0),
                      child: TextCategoryBrand(brandName: "Nike", isSelect: index == 0)
                    ),
                    GestureDetector(
                      onTap: () => selectBrand(1),
                      child: TextCategoryBrand(brandName: "Puma", isSelect: index == 1)
                    ),
                    GestureDetector(
                      onTap: () => selectBrand(2),
                      child: TextCategoryBrand(brandName: "Adidas", isSelect: index == 2)
                    ),
                    GestureDetector(
                      onTap: () => selectBrand(3),
                      child: TextCategoryBrand(brandName: "Reebook", isSelect: index == 3)
                    ),
                    GestureDetector(
                      onTap: () => selectBrand(4),
                      child: TextCategoryBrand(brandName: "Jordan", isSelect: index == 4)
                    ),
                    GestureDetector(
                      onTap: () => selectBrand(5),
                      child: TextCategoryBrand(brandName: "Vans", isSelect: index == 5)
                    ),

                  ],
                ),
              ),
            ),
            //Type Shoes
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget> [
                    GestureDetector(
                      onTap: () => onTapTypeShoe(0),
                      child: const TypeShoes(typeName: "Casual Shoes", imagePath: TypeShoesImage.casual,)
                    ),
                    GestureDetector(
                      onTap: () => onTapTypeShoe(1),
                      child: const TypeShoes(typeName: "Running Shoes", imagePath: TypeShoesImage.running,)),
                    GestureDetector(
                      onTap: () => onTapTypeShoe(2),
                      child: const TypeShoes(typeName: "Basketball Shoes", imagePath: TypeShoesImage.basketball,)),
                    GestureDetector(
                      onTap: () => onTapTypeShoe(3),
                      child: const TypeShoes(typeName: "Training Shoes", imagePath: TypeShoesImage.training,)),
                    GestureDetector(
                      onTap: () => onTapTypeShoe(4),
                      child: const TypeShoes(typeName: "Football Shoes", imagePath: TypeShoesImage.football,)),
                  ],
                ),
              ),
            ), 
          ],
        ),
      ),
    );
  }
}

class TypeShoes extends StatelessWidget {
  const TypeShoes({
    Key? key, required this.typeName, required this.imagePath,
  }) : super(key: key);
  final String typeName;
  final String imagePath;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 30, left: 30, right: 30),
      padding: const EdgeInsets.only(left: 30, bottom: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: const Color(0xFFF3F3F3),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(
            width: 110,
            child: Text(
              typeName,
              style: Theme.of(context).textTheme.headline5,
            ),
          ),
          Image.asset(
            imagePath,
            height: 110,
          ),
        ],
      ),
    );
  }
}

class TextCategoryBrand extends StatelessWidget {
  const TextCategoryBrand({
    Key? key, required this.brandName, required this.isSelect,
  }) : super(key: key);
  final String brandName;
  final bool isSelect;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(right: 20),
      child: Text(
        brandName,
        style: Theme.of(context).textTheme.headline5?.copyWith(color: isSelect? Colors.black : const Color(0xFFB7B7B7)),
      ),
    );
  }
}