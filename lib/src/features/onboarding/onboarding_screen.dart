import 'package:flutter/material.dart';
import 'package:hoang_dinh/src/constants/constans.dart';
import 'package:hoang_dinh/src/features/custom_widget/mybutton_social.dart';
import 'package:hoang_dinh/src/features/custom_widget/textbutton_custom.dart';
import 'package:hoang_dinh/src/features/sign_up/sign_up_screen.dart';
import 'package:hoang_dinh/src/features/onboarding/onboarding_description.dart';

class OnboardingScreen extends StatefulWidget {
  const OnboardingScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<OnboardingScreen> createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  int _pageindex = 0 ;

  void _onTapSignUp(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const SignUpScreen()),
    );
  }

  void _onTapGoogle(){
    print("Tap Google");
  }

  void _onTapFacebook(){
    print("Tap Facebook");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFFFFFF),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              // Description
              Container(
                padding: const EdgeInsets.only(top:30),
                height: 520,
                child: PageView(
                  children: const [
                    OnBoardingDescription(imagepath: imgOnboarding1, content: 'Enjoy The New Arrival Product', subcontent: 'Get your dream item easily and safely with Shoesly. and get other interesting offers',),
                    OnBoardingDescription(imagepath: imgOnboarding2, content: 'Guaranteed Safe Delivery', subcontent: 'Get your dream item easily and safely with Shoesly. and get other interesting offers',),
                    OnBoardingDescription(imagepath: imgOnboarding3, content: 'Goods Arrived On Time', subcontent: 'Get your dream item easily and safely with Shoesly. and get other interesting offers',),                          
                  ],
                  scrollDirection: Axis.horizontal,
                  onPageChanged: (value){
                    setState(() {
                      _pageindex = value;
                    });
                  },
                ),
              ),
              // Process bar
              Container(
                margin: const EdgeInsets.only(bottom: 57),
                padding: const EdgeInsets.only(left: 112, right: 102),
                child: Stack(
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 4,
                          child: Container(
                            height: 2,
                            color: const Color(0xF3F3F3FF),
                          ),
                        ),
                        const Expanded(child: SizedBox()),
                        Expanded(
                          flex: 4,
                          child: Container(
                            height: 2,
                            color: const Color(0xF3F3F3FF),
                          ),
                        ),
                        const Expanded(child: SizedBox(),),
                        Expanded(
                          flex: 4,
                          child: Container(
                            height: 2,
                            color: const Color(0xF3F3F3FF),
                          ),
                        ),
                      ],
                    ),                
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: _pageindex == 0 ? 2 : 4,
                          child: Container(
                            height: 2,
                            color: Colors.black,
                          ),
                        ),
                        Expanded(flex: _pageindex == 0 ? 3 : 1,child: const SizedBox(),),
                        Expanded(
                          flex: _pageindex == 0 ? 0 : _pageindex == 1 ? 2 : 4,
                          child: Container(
                            height: 2,
                            color: Colors.black,
                          ),
                        ),
                        Expanded(flex: _pageindex == 0 ? 5 : _pageindex == 1 ? 5: 1,child: const SizedBox(),),
                        Expanded(
                          flex: _pageindex == 2 ? 2 : 0,
                          child: Container(
                            height: 2,
                            color: Colors.black,
                          ),
                        ),
                        const Expanded(flex: 2,child: SizedBox(),),
                      ],
                    ),                                  
                  ], 
                ),
              ),  
              // Get Started Button
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30).copyWith(bottom: 20),
                child: GestureDetector(
                  onTap: _onTapSignUp,
                  child: TextButtonCustom(text: 'Get Started'.toUpperCase(),),
                ),
              ),
              // Social Button
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 20),
                child: Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () => _onTapGoogle(),
                        child: const MyButtonSocial(
                          icon: iGoogle,
                          text: 'Google',
                        ),
                      )
                    ),
                    const SizedBox(width: 15),
                    Expanded(
                      child: GestureDetector(
                        onTap: () => _onTapFacebook(),
                        child: const MyButtonSocial(
                          icon: iFacebook,
                          text: 'Facebook',
                        ),
                      )
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}


