import 'package:flutter/material.dart';

class OnBoardingDescription extends StatelessWidget {
  const OnBoardingDescription({
    Key? key, required this.imagepath, required this.content, required this.subcontent,
  }) : super(key: key);
  final String imagepath;
  final String content;
  final String subcontent;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 30, bottom: 70),
          child: Image.asset(
            imagepath,
            width: 236,
            height: 200,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 35),
          child: Text(
            content,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline3,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(
            top: 20.0,
            left: 45,
            right: 45,
          ),
          child: Text(
            subcontent,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyText1?.copyWith(color: const Color(0xFF6F6F6F)),
          ),
        ),
      ],
    );
  }
}