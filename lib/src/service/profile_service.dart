import 'dart:convert';

import 'package:hoang_dinh/src/model/User.dart';
import 'package:http/http.dart' as http;

class ProfileService {
  Future<User> getDataUser(String url) async {
    final uri = Uri.parse(url);
    final response = await http.get(uri);

    Map<String, dynamic> result = jsonDecode(response.body);
    var json = result['results'][0];

    return User.fromJson(json);
  }
}