import 'package:flutter/material.dart';
import 'package:hoang_dinh/src/features/onboarding/onboarding_screen.dart';

void main() {
  // runApp(  DevicePreview(
  //   enabled: !kReleaseMode,
  //   builder: (context) => const MyApp(),
  // ),);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //locale: DevicePreview.locale(context),
      //builder: DevicePreview.appBuilder,
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        textTheme: const TextTheme(
          headline3: TextStyle(
            fontSize: 36,
            fontWeight: FontWeight.w600,
            letterSpacing: 1,
            height: 1.3,
            color: Colors.black,
          ),
          headline4: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.w700,
            letterSpacing: 0.3,
            height: 1.5,
            color: Colors.black,
          ),
          headline5: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
            height: 1.5,
            color: Colors.black,
          ),
          headline6: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w700,
            letterSpacing: 1,
            height: 1.3,
            color: Colors.black,
          ),
          bodyText1:  TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w400,
            letterSpacing: 1,
            height: 1.3,
            color: Colors.black,
          ),
          bodyText2:  TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            letterSpacing: 1,
            height: 1.5,
            color: Colors.black,
          ),
        ),
      ),
      home: const OnboardingScreen(),
      );
  }
}

